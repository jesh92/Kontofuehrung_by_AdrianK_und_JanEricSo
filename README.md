############################################################################# [README] ##################################################################

Dies ist die Readme-Datei zum Projekt Kontoführung von Adrian Karonski
und Jan Eric Sohns.

Das Projekt Kontoführung wird ein C-Konsolenprogramm, welches
ein Bankkonto mit folgenden Funktionen simuliert.
____________________________________________________________________________

Einzahlen:	
Hier kann der User einen Betrag x dem Kontostand hinzufügen.
____________________________________________________________________________

Auszahlen:	
Hier kann der User einen Betrag x dem Kontostand abziehen.
____________________________________________________________________________

Kontostand:	
Der User kann sich seinen aktuellen Kontostand anzeigen lassen.
____________________________________________________________________________

Speichern:	
Die Funktion "Speichern" ermöglicht es, dass das Konsolenprogramm 
die von einem User eingetragenen Werte bis zum nächsten Programmstart
beizubehalten (aktueller Kontostand).

___________________________________________________________________________

++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++Eventuelle extra Funktionen++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Kontoinformationen als Text-Datei speichern:	

Das exportieren einer Text-Datei, mit dem aktuellen Kontostand und den 
letzten Einzahlungen/Auszahlungen.
___________________________________________________________________________

Herausforderung:

Die vermutlich größte technische Herausforderung für uns, wird die Funktion
"Speichern", da wir bisher keine vergleichbare Funktion dieser Art kennen
gelernt haben.

Das gleiche gilt für die evtl. extra Funktion "Kontoinformationen als 
Text-Datei speichern".