################################################################# [CONTRIBUTING] #################################################################


Projektbeteiligte sind Adrian Karonski und Jan Eric Sohns.
________________________________________________________________________________

Aufgabenverteilung:
________________________________________________________________________________

Adrian Karonski:	
Ist für die Funktionen "Speichern" und "Auszahlen" zuständig.

________________________________________________________________________________

Jan Eric Sohns:		
Ist für die Funktionen "Einzahlen" und "Kontostand" zuständig.

________________________________________________________________________________

Im Falle, dass die extra Funktion "Kontoinformationen als Text-Datei 
speichern" hinzugefügt wird, werden beide Projektbeteiligten an der 
Umsetztung dieser Funktion arbeiten. 

[siehe README]

________________________________________________________________________________

Programmtests:		
Für das Testen des Programms sind beide Projektbeteiligten zuständig.