#define _CRT_SECURE_NO_WARNINGS
#include <stdlib.h>
#include <math.h>
#include <stdio.h>


int main()
{
	char x, w, k;
	int einzahlung, auszahlung;
	int kontostand = 50;

	printf("Wenn Sie zum aller erstenmal das Programm auf Ihrem Computer starten, bitte mit [n] fuer [nein] als Neukunde bestaetigen, da Sie noch kein Bankkonto besitzen!\n\n");
	printf("================================================================================");
	printf("Haben Sie schon ein Bankkonto bei der Broject Bank? Als Neukunde erhalten Sie ein Willkommensgechenk von 50 Euro! (j/n)\n");
	scanf("%c", &k);
	if (k == 'j') {



		//Einlesen des Kontostandes
		FILE *fp;
		fp = fopen("kontostand.txt", "r");

		if (fp == NULL) {
			printf("Datei konnte nicht geoeffnet werden.\n");
			kontostand = 50;
		}
		else {

			fscanf(fp, "%d", &kontostand);

		}
		fclose(fp);
		//if =j
	}
	else if (k == 'n') {
		kontostand = 150;
		printf("Ihr Startguthaben betraegt %d Euro.", kontostand);
	}
	printf("\n\n");
	printf("					   ____________________________\n");
	printf("					  /			       \\ \n");
	printf("					 /				\\ \n");
	printf("					/________________________________\\ \n");
	printf("					|				 |\n");
	printf("					|WILLKOMMEN BEI DER BROJECT BANK!|\n");
	printf("					|				 |\n");
	printf("========================================================================================================================\n");
	printf("Ihr aktueller Kontostand betraegt %d Euro \n\n", kontostand);
	do {
		printf("Bitte druecken Sie zum Einzahlen [+] und zum Auszahlen [-]. Bestaetigen Sie mit Enter\n");

		//x = getchar();
		scanf(" %c", &x);
		//einzahlung
		if (x == '+') {
			printf("_____________________________________________________________________________________________________________\n");
			printf("Geben Sie den Betrag ein, den Sie einzahlen moechten.\n");
			scanf_s("%d", &einzahlung);
			kontostand = kontostand + einzahlung;
			printf("=============================================================================================================\n");
			printf("Ihr neuer Kontostand betraegt nun %d Euro.\n", kontostand);
			printf("=============================================================================================================\n");

		}
		//auszahlung
		else if (x == '-') {
			printf("_____________________________________________________________________________________________________________\n");
			printf("Geben Sie den Betrag ein, den Sie auszahlen moechten.\n");
			scanf_s("%d", &auszahlung);
			kontostand = kontostand - auszahlung;
			printf("=============================================================================================================\n");
			printf("Bitte entnehmen Sie ihr Geld.\n");
			printf("Ihr neuer Kontostand betraegt nun %d Euro.\n", kontostand);
			printf("=============================================================================================================\n");
		}
		//falscheingabe
		else {
			printf("\n%c ist keine gueltige Eingabe! Bitte geben Sie [+] zum Einzahlen und [-] zum Auszahlen ein.\n", x);
		}
		printf("Moechten Sie eine (weitere) Einzahlung oder Abbuchung taetigen? (j/n)\n");

		scanf(" %c", &w);

	} while (w == 'j');

	//SPEICHERN DES KONTOSTANDES
	FILE*fp;
	fp = fopen("kontostand.txt", "w");
	if (fp == NULL) {
		printf("Datei konnte nicht geoeffnet werden.\n");
	}
	else {
		// schreibe kontostand

		fprintf(fp, "%d\n", kontostand);

		printf("Ihr Konto wurde aktualisiert. Vielen Dank fuer Ihr Vertrauen.\n");
		printf("Besuchen Sie uns bald wieder! Ihre Broject Bank.\n");
		fclose(fp);
	}

}
